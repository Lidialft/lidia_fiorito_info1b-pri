--

-- Database: zz_fiorito_lidia_info1b_dropshipping_5
-- Détection si une autre base de donnée du même nom existe

DROP DATABASE IF EXISTS zz_fiorito_lidia_info1b_dropshipping_5;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS zz_fiorito_lidia_info1b_dropshipping_5;

-- Utilisation de cette base de donnée

USE zz_fiorito_lidia_info1b_dropshipping_5;

-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 12, 2021 at 07:27 PM
-- Server version: 5.7.11
-- PHP Version: 5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zz_fiorito_lidia_info1b_dropshipping`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_order`
--

CREATE TABLE `t_order` (
  `ID_order` int(11) NOT NULL,
  `FK_user` int(11) NOT NULL,
  `Date_creation` date NOT NULL,
  `Date_delivery` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_order`
--

INSERT INTO `t_order` (`ID_order`, `FK_user`, `Date_creation`, `Date_delivery`) VALUES
(1, 3, '2021-05-05', '2021-07-20'),
(2, 3, '2021-05-10', '2021-05-25'),
(3, 1, '2021-05-26', '2021-05-31'),
(4, 2, '2021-05-18', '2021-05-31'),
(5, 4, '2021-05-28', '2021-05-31'),
(6, 2, '2021-05-18', '2021-05-31'),
(8, 4, '2021-05-24', '2021-05-31'),
(9, 3, '2021-05-05', NULL),
(10, 3, '2021-05-05', NULL),
(11, 3, '2021-05-05', NULL),
(12, 3, '2021-05-05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_product`
--

CREATE TABLE `t_product` (
  `ID_product` int(11) NOT NULL,
  `Name_product` varchar(30) NOT NULL,
  `Price_product` int(30) NOT NULL,
  `Description_product` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_product`
--

INSERT INTO `t_product` (`ID_product`, `Name_product`, `Price_product`, `Description_product`) VALUES
(1, 'Face Masksssss', 4, 'Face Mask Blue exfoliantsssss'),
(2, 'Clothes-Short', 15, 'Short red jeans'),
(3, 'Lips Mask GOO', 2, 'Lips Mask exfoliant yellow'),
(4, 'Clothes- Sweat', 28, 'Sweat  white - sun motif cotons'),
(5, 'Eyes Mask', 5, 'Eyes Mask nourishing pink'),
(6, 'Shoes- Basket', 45, 'Basket orange - simple'),
(7, 'Tele', 1200, 'Television plasma'),
(8, 'Platine', 250, 'Platine vinyl'),
(9, 'Radio', 120, 'Radio FM/AM/RDS'),
(10, 'trotinettewwwwwwwwwwwwwww', 45, 'bleu'),
(11, 'test', 50, 'auto');

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE `t_user` (
  `ID_user` int(11) NOT NULL,
  `First_name_user` varchar(30) NOT NULL,
  `Last_name_user` varchar(30) NOT NULL,
  `Mail_user` varchar(30) NOT NULL,
  `Password_user` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`ID_user`, `First_name_user`, `Last_name_user`, `Mail_user`, `Password_user`) VALUES
(1, 'Lidia', 'Fiorito', 'lidia.fiorito@bluewin.ch', 'x-x-x-x-x'),
(2, 'Alissa', 'Fiorito', 'alissa.fiorito@bluewin.ch', 'x-x-x-x-x'),
(3, 'Michel', 'Cachard', 'Michel.Cachard@gmail.com', 'x-x-x-x-x'),
(4, 'Daniel', 'Botta', 'Daniel.Botta@gmail.com', 'x-x-x-x-x');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_order`
--
ALTER TABLE `t_order`
  ADD PRIMARY KEY (`ID_order`),
  ADD KEY `User index` (`FK_user`);

--
-- Indexes for table `t_product`
--
ALTER TABLE `t_product`
  ADD PRIMARY KEY (`ID_product`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`ID_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_order`
--
ALTER TABLE `t_order`
  MODIFY `ID_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `t_product`
--
ALTER TABLE `t_product`
  MODIFY `ID_product` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `t_user`
--
ALTER TABLE `t_user`
  MODIFY `ID_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `t_order`
--
ALTER TABLE `t_order`
  ADD CONSTRAINT `t_order_ibfk_1` FOREIGN KEY (`FK_user`) REFERENCES `t_user` (`ID_user`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
