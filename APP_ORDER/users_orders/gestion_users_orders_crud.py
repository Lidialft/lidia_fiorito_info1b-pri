"""
	Fichier : gestion_product_crud.py
	Auteur : OM 2021.03.16
	Gestions des "routes" FLASK et des données pour les products.
"""

import sys


import pymysql
from flask import flash
from flask import render_template
from flask import request
from flask import session
from flask import url_for
from flask import redirect

from APP_ORDER import obj_mon_application
from APP_ORDER.database.connect_db_context_manager import MaBaseDeDonnee
from APP_ORDER.erreurs.msg_erreurs import *
from APP_ORDER.erreurs.exceptions import *
from APP_ORDER.essais_wtf_forms.wtf_forms_1 import MonPremierWTForm
from APP_ORDER.users_orders.gestion_users_orders_wtf_forms import FormWTFAjouterUserOrder
from APP_ORDER.Products.gestion_Products_wtf_forms import FormWTFDeleteProduct
from APP_ORDER.users_orders.gestion_users_orders_wtf_forms import FormWTFUpdateOrderProduct

"""
	Auteur : OM 2021.03.16
	Définition d'une "route" /product_afficher
	
	Test : ex : http://127.0.0.1:5005/product_afficher
	
	Paramètres : order_by : ASC : Ascendant, DESC : Descendant
				id_user_sel = 0 >> tous les products.
				id_user_sel = "n" affiche le product dont l'id est "n"
"""


@obj_mon_application.route("/user_with_orders_afficher/<int:id_user_sel>", methods=['GET', 'POST'])
def user_with_orders_afficher(id_user_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                code, msg = erreur.args
                flash(f"{error_codes.get(code, msg)} ", "danger")
                flash(f"Exception _init_films_genres_afficher problème de connexion BD : {sys.exc_info()[0]} "
                      f"{erreur.args[0]} , "
                      f"{erreur}", "danger")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_genres_films_afficher_data = """SELECT id_user, Last_name_user, First_name_user,
                                                            GROUP_CONCAT(Name_product) as OrdersUsers FROM t_user_order
                                                            RIGHT JOIN t_user ON t_user.id_user = t_user_order.fk_user
                                                            LEFT JOIN t_product ON t_product.id_product = t_user_order.fk_product
                                                            GROUP BY id_order"""
                if id_user_sel == 0:
                    # le paramètre 0 permet d'afficher tous les films
                    # Sinon le paramètre représente la valeur de l'id du film
                    mc_afficher.execute(strsql_genres_films_afficher_data)
                else:
                    # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                    valeur_id_film_selected_dictionnaire = {"value_id_film_selected": id_user_sel}
                    # En MySql l'instruction HAVING fonctionne comme un WHERE... mais doit être associée à un GROUP BY
                    # L'opérateur += permet de concaténer une nouvelle valeur à la valeur de gauche préalablement définie.
                    strsql_genres_films_afficher_data += """ HAVING id_user= %(value_id_film_selected)s"""

                    mc_afficher.execute(strsql_genres_films_afficher_data, valeur_id_film_selected_dictionnaire)

                # Récupère les données de la requête.
                data_genres_films_afficher = mc_afficher.fetchall()
                print("data_genres ", data_genres_films_afficher, " Type : ", type(data_genres_films_afficher))

                # Différencier les messages.
                if not data_genres_films_afficher and id_user_sel == 0:
                    flash("""La table "t_film" est vide. !""", "warning")
                elif not data_genres_films_afficher and id_user_sel > 0:
                    # Si l'utilisateur change l'id_film dans l'URL et qu'il ne correspond à aucun film
                    flash(f"Le film {id_user_sel} demandé n'existe pas !!", "warning")
                else:
                    flash(f"Données films et genres affichés !!", "success")

        except Exception as Exception_films_genres_afficher:
            code, msg = Exception_films_genres_afficher.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception films_genres_afficher : {sys.exc_info()[0]} "
                  f"{Exception_films_genres_afficher.args[0]} , "
                  f"{Exception_films_genres_afficher}", "danger")

    # Envoie la page "HTML" au serveur.

    return render_template("users_orders/user_with_orders_afficher.html", data=data_genres_films_afficher)



@obj_mon_application.route("/films_genres_afficher/<int:id_film_sel>", methods=['GET', 'POST'])
def users_products_afficher(id_film_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                code, msg = erreur.args
                flash(f"{error_codes.get(code, msg)} ", "danger")
                flash(f"Exception _init_films_genres_afficher problème de connexion BD : {sys.exc_info()[0]} "
                      f"{erreur.args[0]} , "
                      f"{erreur}", "danger")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_genres_films_afficher_data = """SELECT id_user, Last_name_user, First_name_user,
                                                            GROUP_CONCAT(Name_product) as GenresFilms FROM t_user_order
                                                            RIGHT JOIN t_user ON t_user.id_order = t_user_order.fk_user
                                                            LEFT JOIN t_product ON t_product.id_product = t_user_order.fk_product
                                                            GROUP BY id_user"""
                if id_film_sel == 0:
                    # le paramètre 0 permet d'afficher tous les films
                    # Sinon le paramètre représente la valeur de l'id du film
                    mc_afficher.execute(strsql_genres_films_afficher_data)
                else:
                    # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                    valeur_id_film_selected_dictionnaire = {"value_id_film_selected": id_film_sel}
                    # En MySql l'instruction HAVING fonctionne comme un WHERE... mais doit être associée à un GROUP BY
                    # L'opérateur += permet de concaténer une nouvelle valeur à la valeur de gauche préalablement définie.
                    strsql_genres_films_afficher_data += """ HAVING id_film= %(value_id_film_selected)s"""

                    mc_afficher.execute(strsql_genres_films_afficher_data, valeur_id_film_selected_dictionnaire)

                # Récupère les données de la requête.
                data_genres_films_afficher = mc_afficher.fetchall()
                print("data_genres ", data_genres_films_afficher, " Type : ", type(data_genres_films_afficher))

                # Différencier les messages.
                if not data_genres_films_afficher and id_film_sel == 0:
                    flash("""La table "t_film" est vide. !""", "warning")
                elif not data_genres_films_afficher and id_film_sel > 0:
                    # Si l'utilisateur change l'id_film dans l'URL et qu'il ne correspond à aucun film
                    flash(f"Le film {id_film_sel} demandé n'existe pas !!", "warning")
                else:
                    flash(f"Données films et genres affichés !!", "success")

        except Exception as Exception_films_genres_afficher:
            code, msg = Exception_films_genres_afficher.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception films_genres_afficher : {sys.exc_info()[0]} "
                  f"{Exception_films_genres_afficher.args[0]} , "
                  f"{Exception_films_genres_afficher}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template("users_orders/users_order_afficher.html", data=data_genres_films_afficher)



@obj_mon_application.route("/users_orders_afficher/<string:order_by>/<int:id_user_sel>", methods=['GET', 'POST'])
def users_orders_afficher(order_by, id_user_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans GestionProduct ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionProduct {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_user_sel == 0:
                    strsql_products_afficher = """SELECT tuo.FK_user, tuo.ID_order, tuo.FK_product, tp.Name_product FROM t_user_order tuo  INNER JOIN t_product tp on tp.id_product = tuo.FK_product ORDER BY tuo.ID_order DESC"""
                    mc_afficher.execute(strsql_products_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_genre"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du genre sélectionné avec un nom de variable
                    valeur_ID_user_selected_dictionnaire = {"value_ID_user_selected": id_user_sel}
                    strsql_users_afficher = """SELECT FK_user, ID_order  FROM t_user_order  WHERE ID_user = %(value_ID_user_selected)s"""

                    mc_afficher.execute(strsql_users_afficher, valeur_ID_user_selected_dictionnaire)
                else:
                    strsql_products_afficher = """SELECT tuo.FK_user, tuo.ID_order,  tuo.FK_product, tp.Name_product FROM t_user_order tuo  INNER JOIN t_product tp on tp.id_product = tuo.FK_product ORDER BY tuo.ID_order DESC"""

                    mc_afficher.execute(strsql_products_afficher)

                data_products = mc_afficher.fetchall()

                print("data_product ", data_products, " Type : ", type(data_products))

                # Différencier les messages si la table est vide.
                if not data_products and id_user_sel == 0:
                    flash("""La table "t_product" est vide. !!""", "warning")
                elif not data_products and id_user_sel > 0:
                    # Si l'utilisateur change l'id_product dans l'URL et que le genre n'existe pas,
                    flash(f"Le product demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_product" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données products affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")
            raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("users_orders/users_orders_afficher.html", data=data_products)


"""
	Auteur : OM 2021.03.22
	Définition d'une "route" /products_ajouter
	
	Test : ex : http://127.0.0.1:5005/products_ajouter
	
	Paramètres : sans
	
	But : Ajouter un product pour un user
	
	Remarque :  Dans le champ "name_product_html" du formulaire "products/products_ajouter.html",
				le contrôle de la saisie s'effectue ici en Python.
				On transforme la saisie en minuscules.
				On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
				des valeurs avec des caractères qui ne sont pas des lettres.
				Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
				Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/user_order_ajouter/<int:id_user_sel>", methods=['GET', 'POST'])
def user_order_ajouter_wtf( id_user_sel):
    form = FormWTFAjouterUserOrder()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion products ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionProducts {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                ID_product_wtf = form.ID_product_wtf
                ID_order_wtf = form.ID_order_wtf
                ID_user_wtf = form.ID_user_wtf
                valeurs_insertion_dictionnaire = {"value_id_product":  ID_product_wtf,
                                                  "value_id_order": ID_order_wtf,
                                                  "value_id_user": ID_user_wtf,
                                                  }
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_product = """INSERT INTO t_user_order (ID_order, FK_product , FK_user  ) VALUES (NULL,%(value_id_product)s , %(value_id_order)s, %(value_id_user)s  )"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_product, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('users_orders_afficher', order_by='DESC', id_user_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_product_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_product_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion products CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("users_orders/user_order_ajouter_wtf.html", form=form)



@obj_mon_application.route("/edit_products_user_selected", methods=['GET', 'POST'])
def edit_products_user_selected():
    if request.method == "GET":
        try:
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_genres_afficher = """SELECT id_product, Name_product FROM t_product ORDER BY id_product ASC"""
                mc_afficher.execute(strsql_genres_afficher)
            data_genres_all = mc_afficher.fetchall()
            print("dans edit_genre_film_selected ---> data_genres_all", data_genres_all)

            # Récupère la valeur de "id_film" du formulaire html "films_genres_afficher.html"
            # l'utilisateur clique sur le bouton "Modifier" et on récupère la valeur de "id_film"
            # grâce à la variable "id_film_genres_edit_html" dans le fichier "films_genres_afficher.html"
            # href="{{ url_for('edit_genre_film_selected', id_film_genres_edit_html=row.id_film) }}"
            id_film_genres_edit = request.values['id_film_genres_edit_html']

            # Mémorise l'id du film dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_film_genres_edit'] = id_film_genres_edit

            # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
            valeur_id_film_selected_dictionnaire = {"value_id_film_selected": id_film_genres_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la fonction genres_films_afficher_data
            # 1) Sélection du film choisi
            # 2) Sélection des genres "déjà" attribués pour le film.
            # 3) Sélection des genres "pas encore" attribués pour le film choisi.
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "genres_films_afficher_data"
            data_genre_film_selected, data_genres_films_non_attribues, data_genres_films_attribues = \
                products_user_afficher_data(valeur_id_film_selected_dictionnaire)

            print(data_genre_film_selected)
            lst_data_film_selected = [item['ID_user'] for item in data_genre_film_selected]
            print("lst_data_film_selected  ", lst_data_film_selected,
                  type(lst_data_film_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les genres qui ne sont pas encore sélectionnés.
            lst_data_genres_films_non_attribues = [item['id_product'] for item in data_genres_films_non_attribues]
            session['session_lst_data_genres_films_non_attribues'] = lst_data_genres_films_non_attribues
            print("lst_data_genres_films_non_attribues  ", lst_data_genres_films_non_attribues,
                  type(lst_data_genres_films_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les genres qui sont déjà sélectionnés.
            lst_data_genres_films_old_attribues = [item['id_product'] for item in data_genres_films_attribues]
            session['session_lst_data_genres_films_old_attribues'] = lst_data_genres_films_old_attribues
            print("lst_data_genres_films_old_attribues  ", lst_data_genres_films_old_attribues,
                  type(lst_data_genres_films_old_attribues))

            print(" data data_genre_film_selected", data_genre_film_selected, "type ", type(data_genre_film_selected))
            print(" data data_genres_films_non_attribues ", data_genres_films_non_attribues, "type ",
                  type(data_genres_films_non_attribues))
            print(" data_genres_films_attribues ", data_genres_films_attribues, "type ",
                  type(data_genres_films_attribues))

            # Extrait les valeurs contenues dans la table "t_genres", colonne "intitule_genre"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_genre
            lst_data_genres_films_non_attribues = [item['Name_product'] for item in data_genres_films_non_attribues]
            print("lst_all_genres gf_edit_genre_film_selected ", lst_data_genres_films_non_attribues,
                  type(lst_data_genres_films_non_attribues))

        except Exception as Exception_edit_genre_film_selected:
            code, msg = Exception_edit_genre_film_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception edit_genre_film_selected : {sys.exc_info()[0]} "
                  f"{Exception_edit_genre_film_selected.args[0]} , "
                  f"{Exception_edit_genre_film_selected}", "danger")

    return render_template("users_orders/user_orders_modifier_tags_dropbox.html",
                           data_genres=data_genres_all,
                           data_user_selected=data_genre_film_selected,
                           data_genres_attribues=data_genres_films_attribues,
                           data_genres_non_attribues=data_genres_films_non_attribues)



def products_user_afficher_data(valeur_id_film_selected_dict):
    print("valeur_id_film_selected_dict...", valeur_id_film_selected_dict)
    try:

        strsql_film_selected = """SELECT ID_user, First_name_user, Last_name_user, GROUP_CONCAT(ID_product) as GenresFilms FROM t_user_order
                                        INNER JOIN t_user ON t_user.ID_user = t_user_order.FK_user
                                        INNER JOIN t_product ON t_product.ID_product = t_user_order.FK_product
                                        WHERE ID_user = %(value_id_film_selected)s"""

        strsql_products_user_non_attribues = """SELECT id_product, Name_product FROM t_product WHERE id_product not in(SELECT id_product as idGenresFilms FROM t_user_order
                                                    INNER JOIN t_user ON t_user.ID_user = t_user_order.fk_user
                                                    INNER JOIN t_product ON t_product.id_product = t_user_order.fk_product
                                                    WHERE id_user = %(value_id_film_selected)s)"""

        strsql_genres_films_attribues = """SELECT id_user, id_product, Name_product FROM t_user_order
                                             INNER JOIN t_user ON t_user.id_user = t_user_order.fk_user
                                            INNER JOIN t_product ON t_product.id_product = t_user_order.fk_product
                                            WHERE id_user = %(value_id_film_selected)s"""

        # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
        with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
            # Envoi de la commande MySql
            mc_afficher.execute(strsql_products_user_non_attribues, valeur_id_film_selected_dict)
            # Récupère les données de la requête.
            data_genres_films_non_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("genres_films_afficher_data ----> data_genres_films_non_attribues ", data_genres_films_non_attribues,
                  " Type : ",
                  type(data_genres_films_non_attribues))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_film_selected, valeur_id_film_selected_dict)
            # Récupère les données de la requête.
            data_film_selected = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_film_selected  ", data_film_selected, " Type : ", type(data_film_selected))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_genres_films_attribues, valeur_id_film_selected_dict)
            # Récupère les données de la requête.
            data_genres_films_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_genres_films_attribues ", data_genres_films_attribues, " Type : ",
                  type(data_genres_films_attribues))

            # Retourne les données des "SELECT"
            return data_film_selected, data_genres_films_non_attribues, data_genres_films_attribues
    except pymysql.Error as pymysql_erreur:
        code, msg = pymysql_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.Error Erreur dans genres_films_afficher_data : {sys.exc_info()[0]} "
              f"{pymysql_erreur.args[0]} , "
              f"{pymysql_erreur}", "danger")
    except Exception as exception_erreur:
        code, msg = exception_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"Exception Erreur dans genres_films_afficher_data : {sys.exc_info()[0]} "
              f"{exception_erreur.args[0]} , "
              f"{exception_erreur}", "danger")
    except pymysql.err.IntegrityError as IntegrityError_genres_films_afficher_data:
        code, msg = IntegrityError_genres_films_afficher_data.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.err.IntegrityError Erreur dans genres_films_afficher_data : {sys.exc_info()[0]} "
              f"{IntegrityError_genres_films_afficher_data.args[0]} , "
              f"{IntegrityError_genres_films_afficher_data}", "danger")




@obj_mon_application.route("/user_order_update", methods=['GET', 'POST'])
def user_order_update_wtf():

    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_user"
    id_order_update = request.values['id_order_selected_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateOrderProduct()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "genre_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.

            valeur_update_dictionnaire = { "value_ID_product": id_order_update ,
                                            "value_Name_product": Name_product_update
                                           }
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intitulegenre = """UPDATE t_product SET Name_product = %(value_Name_product)s , Price_product = %(value_Price_product)s , Description_product = %(value_Description_product)s  WHERE id_product = %(value_ID_product)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulegenre, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_product_update"
            return redirect(url_for('users_orders_afficher', order_by="ASC", id_user_sel=id_product_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_genre" et "intitule_genre" de la "t_genre"
            str_sql_id_Product = "SELECT ID_product, Name_product , Price_product , Description_product  FROM t_product WHERE id_product = %(value_ID_product)s"
            valeur_select_dictionnaire = {"value_ID_product": id_order_update ,
                                          "value_Name_product": Name_product_update
                                          }
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_Product , valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom product" pour l'UPDATE
            data_user = mybd_curseur.fetchone()


            # Afficher la valeur sélectionnée dans le champ du formulaire "genre_update_wtf.html"
            form_update.Name_product_wtf.data = data_user["Name_product"]
            form_update.Price_product_wtf.data = data_user["Price_product"]
            form_update.Description_product_wtf.data = data_user["Description_product"]
            form_update.ID_product_wtf.data = data_user["ID_product"]


    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans product_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans product_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans product_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans product_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("Products/Product_update_wtf.html", form=form_update)



"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /product_delete

    Test : ex. cliquer sur le menu "users" puis cliquer sur le bouton "DELETE" d'un "PRODUCT"

    Paramètres : sans

    But : Effacer(delete) un product qui a été sélectionné dans le formulaire "products_afficher.html"

    Remarque :  Dans le champ "ID_product_delete_wtf" du formulaire "Products/Product_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/user_order_delete", methods=['GET', 'POST'])
def user_order_delete_wtf():
    data_nom_attribue_product_delete = None
    submit_btn_del_visible = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_product"
    id_product_delete = request.values['id_product_btn_delete_html']
    print(id_product_delete);

    # Objet formulaire pour effacer le genre sélectionné.
    form_delete = FormWTFDeleteProduct()
    try:
        print(" on submit  delete", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("products_afficher", order_by="ASC", id_product_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "Products/gestion_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_nom_product = session['data_product_delete']
                print("data_films_attribue_product_delete ", data_nom_product)

                flash(f"Effacer le product de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer product" qui va irrémédiablement EFFACER le product
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_product": id_product_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_id_product = """DELETE FROM t_product WHERE ID_product = %(value_id_product)s"""
                # Ensuite on peut effacer le product vu qu'il n'est plus "lié" (INNODB) dans la "t_genre_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_id_product, valeur_delete_dictionnaire)

                flash(f"Product définitivement effacé !!", "success")
                print(f"Product définitivement effacé !!")

                # afficher les données
                return redirect(url_for('products_afficher', order_by="ASC", id_product_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_product": id_product_delete}
            print(id_product_delete, type(id_product_delete))

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            # Opération sur la BD pour récupérer "id_genre" et "intitule_genre" de la "t_genre"
            str_sql_id_product = "SELECT ID_product, Name_product, Price_product, Description_product FROM t_product WHERE ID_product = %(value_id_product)s"

            mybd_curseur.execute(str_sql_id_product, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom genre" pour l'action DELETE
            data_nom_product = mybd_curseur.fetchone()
            print("data_nom_product ", data_nom_product, " type ", type(data_nom_product), " product ",
                  data_nom_product["Name_product"])

            session['data_product_delete'] = data_nom_product

            # Afficher la valeur sélectionnée dans le champ du formulaire "genre_delete_wtf.html"
            form_delete.Name_product_delete_wtf.data = data_nom_product["Name_product"]
            form_delete.Price_product_delete_wtf.data = data_nom_product["Price_product"]
            form_delete.Description_product_delete_wtf.data = data_nom_product["Description_product"]
            form_delete.ID_product_delete_wtf.data = data_nom_product["ID_product"]

            # Le bouton pour l'action "DELETE" dans le form. "product_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans Product_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans Product_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans Product_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans Product_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("Products/Product_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_films_associes=data_nom_product)

