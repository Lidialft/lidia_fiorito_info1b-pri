"""
    Fichier : gestion_users_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""

from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import IntegerField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterUsers(FlaskForm):
    """
        Dans le formulaire "users_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    FirstName_user_wtf = StringField("FirstName")
    LastName_user_wtf = StringField("LastName")
    Mail_user_wtf = StringField("Email")
    Password_user_wtf = StringField("Password")

    submit = SubmitField("Enregistrer user")

class FormWTFUpdateUsers(FlaskForm):
    """
        Dans le formulaire "users_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    FirstName_user_wtf = StringField("FirstName")
    LastName_user_wtf = StringField("LastName")
    Mail_user_wtf = StringField("Email")
    Password_user_wtf = StringField("Password")

    submit = SubmitField("Mise à jour user")

class FormWTFDeleteUsers(FlaskForm):
    """
        Dans le formulaire "User_delete_wtf.html"

        ID_user_delete_wtf : Champ qui reçoit la valeur du user, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "user".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_user".
    """

    FirstName_user_delete_wtf = StringField("FirstName")
    LastName_user_delete_wtf = StringField("LastName")
    Mail_user_delete_wtf = StringField("Email")
    Password_user_delete_wtf = StringField("Password")
    ID_user_delete_wtf = IntegerField("Effacer ce user")

    submit_btn_del = SubmitField("Effacer user")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")



