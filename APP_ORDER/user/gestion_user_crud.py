"""
    Fichier : gestion_users_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les user.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for
from flask import redirect

from APP_ORDER import obj_mon_application
from APP_ORDER.database.connect_db_context_manager import MaBaseDeDonnee
from APP_ORDER.erreurs.exceptions import *
from APP_ORDER.erreurs.msg_erreurs import *
from APP_ORDER.user.gestion_user_wtf_forms import FormWTFAjouterUsers
from APP_ORDER.user.gestion_user_wtf_forms import FormWTFDeleteUsers
from APP_ORDER.user.gestion_user_wtf_forms import FormWTFUpdateUsers

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /users_afficher
    
    Test : ex : http://127.0.0.1:5005/users_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_user_sel = 0 >> tous les user.
                id_user_sel = "n" affiche le user dont l'id est "n"
"""



@obj_mon_application.route("/users_afficher/<string:order_by>/<int:id_user_sel>", methods=['GET', 'POST'])
def users_afficher(order_by, id_user_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion users ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionUsers {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_user_sel == 0:
                    strsql_genres_afficher = """SELECT * FROM t_user """
                    mc_afficher.execute(strsql_genres_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_genre"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du genre sélectionné avec un nom de variable
                    valeur_id_users_selected_dictionnaire = {"value_id_user_selected": id_user_sel}
                    strsql_users_afficher = """SELECT ID_user, First_name_user, Last_name_user, Mail_user, Password_user Last FROM t_user  WHERE ID_user = %(value_id_user_selected)s"""

                    mc_afficher.execute(strsql_users_afficher, valeur_id_users_selected_dictionnaire)
                else:
                    strsql_users_afficher = """SELECT ID_user, First_name_user, Last_name_user, Mail_user, Password_user FROM t_user ORDER BY ID_user DESC"""

                    mc_afficher.execute(strsql_users_afficher)

                data_users = mc_afficher.fetchall()

                print("data_users ", data_users, " Type : ", type(data_users))

                # Différencier les messages si la table est vide.
                if not data_users and id_user_sel == 0:
                    flash("""La table "t_user" est vide. !!""", "warning")
                elif not data_users and id_user_sel > 0:
                    # Si l'utilisateur change l'id_user dans l'URL et que le genre n'existe pas,
                    flash(f"Le user demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_genre" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données users affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")
            raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("User/User_afficher.html", data=data_users)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /users_ajouter
    
    Test : ex : http://127.0.0.1:5005/users_ajouter
    
    Paramètres : sans
    
    But : Ajouter un user pour un order
    
    Remarque :  Dans le champ "name_user_html" du formulaire "User/users_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/users_ajouter", methods=['GET', 'POST'])
def users_ajouter_wtf():
    form = FormWTFAjouterUsers()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion users ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionUsers {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")


            if form.validate_on_submit():
                firstname_user = form.FirstName_user_wtf.data
                lastname_user = form.LastName_user_wtf.data
                mail_user = form.Mail_user_wtf.data
                password_user = form.Password_user_wtf.data

                valeurs_insertion_dictionnaire = {"value_firstname_user": firstname_user,
                                                  "value_lastname_user": lastname_user,
                                                  "value_mail_user": mail_user,
                                                  "value_password_user": password_user,
                                                  }
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_user = """INSERT INTO t_user (ID_user,First_name_user, Last_name_user, Mail_user, Password_user) VALUES (NULL,%(value_firstname_user)s,%(value_lastname_user)s, %(value_mail_user)s,%(value_password_user)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_user, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('users_afficher', order_by='DESC', id_user_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_user_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_user_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion users CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("User/User_ajouter_wtf.html", form=form)


@obj_mon_application.route("/User_delete", methods=['GET', 'POST'])
def User_delete_wtf():
    data_films_attribue_genre_delete = None
    submit_btn_del_visible = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_user"
    id_user_delete = request.values['id_user_btn_delete_html']
    print(id_user_delete);

    # Objet formulaire pour effacer le genre sélectionné.
    form_delete = FormWTFDeleteUsers()
    try:
        print(" on submit  delete", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("users_afficher", order_by="ASC", id_user_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "Users/gestion_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_nom_user = session['data_user_delete']
                print("data_films_attribue_user_delete ", data_nom_user)

                flash(f"Effacer le user de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer user" qui va irrémédiablement EFFACER le user
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_user": id_user_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_id_user = """DELETE FROM t_user WHERE ID_user = %(value_id_user)s"""
                # Ensuite on peut effacer le genre vu qu'il n'est plus "lié" (INNODB) dans la "t_genre_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_id_user, valeur_delete_dictionnaire)

                flash(f"User définitivement effacé !!", "success")
                print(f"User définitivement effacé !!")

                # afficher les données
                return redirect(url_for('users_afficher', order_by="ASC", id_user_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_user": id_user_delete}
            print(id_user_delete, type(id_user_delete))

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            # Opération sur la BD pour récupérer "id_genre" et "intitule_genre" de la "t_genre"
            str_sql_id_user = "SELECT ID_user, First_name_user, Last_name_user, Mail_user, Password_user FROM t_user WHERE ID_user = %(value_id_user)s"

            mybd_curseur.execute(str_sql_id_user, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom genre" pour l'action DELETE
            data_nom_user = mybd_curseur.fetchone()
            print("data_nom_user ", data_nom_user, " type ", type(data_nom_user), " user ",
                  data_nom_user["First_name_user"])

            session['data_user_delete'] = data_nom_user

            # Afficher la valeur sélectionnée dans le champ du formulaire "genre_delete_wtf.html"
            form_delete.FirstName_user_delete_wtf.data = data_nom_user["First_name_user"]
            form_delete.LastName_user_delete_wtf.data = data_nom_user["Last_name_user"]
            form_delete.Mail_user_delete_wtf.data = data_nom_user["Mail_user"]
            form_delete.Password_user_delete_wtf.data = data_nom_user["Password_user"]
            form_delete.ID_user_delete_wtf.data = data_nom_user["ID_user"]

            # Le bouton pour l'action "DELETE" dans le form. "genre_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans User_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans User_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans User_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans User_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("User/User_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_films_associes=data_nom_user)




@obj_mon_application.route("/user_update", methods=['GET', 'POST'])
def user_update_wtf():

    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_user"
    id_user_update = request.values['id_user_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateUsers()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "genre_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            First_name_user_update = form_update.FirstName_user_wtf.data
            Last_name_user_update = form_update.LastName_user_wtf.data
            Mail_user_update = form_update.Mail_user_wtf.data
            Password_user_update = form_update.Password_user_wtf.data

            valeur_update_dictionnaire = { "value_First_name_user": First_name_user_update,
                                           "value_Last_name_user": Last_name_user_update,
                                           "value_Mail_user": Mail_user_update,
                                           "value_Password_user": Password_user_update,
                                           "value_ID_user": id_user_update
                                           }
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intitulegenre = """UPDATE t_user SET First_name_user = %(value_First_name_user)s , Last_name_user = %(value_Last_name_user)s , Mail_user = %(value_Mail_user)s , Password_user = %(value_Password_user)s WHERE id_user = %(value_ID_user)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulegenre, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_genre_update"
            return redirect(url_for('users_afficher', order_by="ASC", id_user_sel=id_user_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_genre" et "intitule_genre" de la "t_genre"
            str_sql_id_user = "SELECT ID_user, First_name_user , Last_name_user , Mail_user , Password_user FROM t_user WHERE id_user = %(value_ID_user)s"
            valeur_select_dictionnaire = {"value_ID_user": id_user_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_user, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom genre" pour l'UPDATE
            data_user = mybd_curseur.fetchone()


            # Afficher la valeur sélectionnée dans le champ du formulaire "genre_update_wtf.html"
            form_update.FirstName_user_wtf.data = data_user["ID_user"]
            form_update.FirstName_user_wtf.data = data_user["First_name_user"]
            form_update.LastName_user_wtf.data = data_user["Last_name_user"]
            form_update.Mail_user_wtf.data = data_user["Mail_user"]
            form_update.Password_user_wtf.data = data_user["Password_user"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans user_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans user_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans user_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans user_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("User/User_update_wtf.html",
                           form=form_update)



"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /users_ajouter

    Test : ex : http://127.0.0.1:5005/users_ajouter

    Paramètres : sans

    But : Ajouter un user pour un order

    Remarque :  Dans le champ "name_user_html" du formulaire "User/users_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


