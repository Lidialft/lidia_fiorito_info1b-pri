"""
	Fichier : gestion_product_crud.py
	Auteur : OM 2021.03.16
	Gestions des "routes" FLASK et des données pour les products.
"""

import sys

from flask import flash
from flask import render_template
from flask import request
from flask import url_for
from flask import redirect

from APP_ORDER import obj_mon_application
from APP_ORDER.database.connect_db_context_manager import MaBaseDeDonnee
from APP_ORDER.erreurs.msg_erreurs import *
from APP_ORDER.erreurs.exceptions import *
from APP_ORDER.essais_wtf_forms.wtf_forms_1 import MonPremierWTForm
from APP_ORDER.Products.gestion_Products_wtf_forms import FormWTFAjouterProducts
from APP_ORDER.Products.gestion_Products_wtf_forms import FormWTFDeleteProduct
from APP_ORDER.Products.gestion_Products_wtf_forms import FormWTFUpdateProduct

"""
	Auteur : OM 2021.03.16
	Définition d'une "route" /product_afficher
	
	Test : ex : http://127.0.0.1:5005/product_afficher
	
	Paramètres : order_by : ASC : Ascendant, DESC : Descendant
				id_product_sel = 0 >> tous les products.
				id_product_sel = "n" affiche le product dont l'id est "n"
"""


@obj_mon_application.route("/products_afficher/<string:order_by>/<int:id_product_sel>", methods=['GET', 'POST'])
def products_afficher(order_by, id_product_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans GestionProduct ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionProduct {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_product_sel == 0:
                    strsql_products_afficher = """SELECT * FROM t_product"""
                    mc_afficher.execute(strsql_products_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_genre"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du genre sélectionné avec un nom de variable
                    valeur_ID_product_selected_dictionnaire = {"value_ID_product_selected": id_product_sel}
                    strsql_products_afficher = """SELECT ID_product, Name_product, Price_product, Description_product FROM t_product  WHERE ID_product = %(value_ID_product_selected)s"""

                    mc_afficher.execute(strsql_products_afficher, valeur_ID_product_selected_dictionnaire)
                else:
                    strsql_products_afficher = """SELECT ID_product, Name_product, Price_product, Description_product FROM t_product ORDER BY ID_product DESC"""

                    mc_afficher.execute(strsql_products_afficher)

                data_products = mc_afficher.fetchall()

                print("data_product ", data_products, " Type : ", type(data_products))

                # Différencier les messages si la table est vide.
                if not data_products and id_product_sel == 0:
                    flash("""La table "t_product" est vide. !!""", "warning")
                elif not data_products and id_product_sel > 0:
                    # Si l'utilisateur change l'id_product dans l'URL et que le genre n'existe pas,
                    flash(f"Le product demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_product" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données products affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")
            raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("Products/Products_afficher.html", data=data_products)


"""
	Auteur : OM 2021.03.22
	Définition d'une "route" /products_ajouter
	
	Test : ex : http://127.0.0.1:5005/products_ajouter
	
	Paramètres : sans
	
	But : Ajouter un product pour un user
	
	Remarque :  Dans le champ "name_product_html" du formulaire "products/products_ajouter.html",
				le contrôle de la saisie s'effectue ici en Python.
				On transforme la saisie en minuscules.
				On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
				des valeurs avec des caractères qui ne sont pas des lettres.
				Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
				Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/products_ajouter", methods=['GET', 'POST'])
def products_ajouter_wtf():
    form = FormWTFAjouterProducts()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion products ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionProducts {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                Name_product_wtf = form.Name_product_wtf.data
                Price_product_wtf = form.Price_product_wtf.data
                Description_product_wtf = form.Description_product_wtf.data

                valeurs_insertion_dictionnaire = {"value_Name_product": Name_product_wtf,
                                                  "value_Price_product": Price_product_wtf,
                                                  "value_Description_product": Description_product_wtf,
                                                  }

                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_product = """INSERT INTO t_product (ID_product, Name_product , Price_product , Description_product  ) VALUES (NULL,%(value_Name_product)s , %(value_Price_product)s , %(value_Description_product)s )"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_product, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('products_afficher', order_by='DESC', id_product_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_product_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_product_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion products CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("Products/Products_ajouter_wtf.html", form=form)


@obj_mon_application.route("/product_update", methods=['GET', 'POST'])
def product_update_wtf():

    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_user"
    id_product_update = request.values['id_product_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateProduct()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "genre_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            id_product_update = form_update.ID_product_wtf.data
            Name_product_update = form_update.Name_product_wtf.data
            Price_product_update = form_update.Price_product_wtf.data
            Description_product_update = form_update.Description_product_wtf.data


            valeur_update_dictionnaire = { "value_ID_product": id_product_update,
                                           "value_Name_product": Name_product_update,
                                           "value_Price_product": Price_product_update,
                                           "value_Description_product": Description_product_update
                                           }
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intitulegenre = """UPDATE t_product SET Name_product = %(value_Name_product)s , Price_product = %(value_Price_product)s , Description_product = %(value_Description_product)s  WHERE id_product = %(value_ID_product)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulegenre, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_product_update"
            return redirect(url_for('products_afficher', order_by="ASC", id_product_sel=id_product_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_genre" et "intitule_genre" de la "t_genre"
            str_sql_id_Product = "SELECT ID_product, Name_product , Price_product , Description_product  FROM t_product WHERE id_product = %(value_ID_product)s"
            valeur_select_dictionnaire = {"value_ID_product": id_product_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_Product , valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom product" pour l'UPDATE
            data_user = mybd_curseur.fetchone()


            # Afficher la valeur sélectionnée dans le champ du formulaire "genre_update_wtf.html"
            form_update.Name_product_wtf.data = data_user["Name_product"]
            form_update.Price_product_wtf.data = data_user["Price_product"]
            form_update.Description_product_wtf.data = data_user["Description_product"]
            form_update.ID_product_wtf.data = data_user["ID_product"]


    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans product_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans product_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans product_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans product_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("Products/Product_update_wtf.html", form=form_update)



"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /product_delete

    Test : ex. cliquer sur le menu "users" puis cliquer sur le bouton "DELETE" d'un "PRODUCT"

    Paramètres : sans

    But : Effacer(delete) un product qui a été sélectionné dans le formulaire "products_afficher.html"

    Remarque :  Dans le champ "ID_product_delete_wtf" du formulaire "Products/Product_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/Product_delete", methods=['GET', 'POST'])
def Product_delete_wtf():
    data_nom_attribue_product_delete = None
    submit_btn_del_visible = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_product"
    id_product_delete = request.values['id_product_btn_delete_html']
    print(id_product_delete);

    # Objet formulaire pour effacer le genre sélectionné.
    form_delete = FormWTFDeleteProduct()
    try:
        print(" on submit  delete", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("products_afficher", order_by="ASC", id_product_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "Products/gestion_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_nom_product = session['data_product_delete']
                print("data_films_attribue_product_delete ", data_nom_product)

                flash(f"Effacer le product de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer product" qui va irrémédiablement EFFACER le product
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_product": id_product_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_id_product = """DELETE FROM t_product WHERE ID_product = %(value_id_product)s"""
                # Ensuite on peut effacer le product vu qu'il n'est plus "lié" (INNODB) dans la "t_genre_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_id_product, valeur_delete_dictionnaire)

                flash(f"Product définitivement effacé !!", "success")
                print(f"Product définitivement effacé !!")

                # afficher les données
                return redirect(url_for('products_afficher', order_by="ASC", id_product_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_product": id_product_delete}
            print(id_product_delete, type(id_product_delete))

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            # Opération sur la BD pour récupérer "id_genre" et "intitule_genre" de la "t_genre"
            str_sql_id_product = "SELECT ID_product, Name_product, Price_product, Description_product FROM t_product WHERE ID_product = %(value_id_product)s"

            mybd_curseur.execute(str_sql_id_product, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom genre" pour l'action DELETE
            data_nom_product = mybd_curseur.fetchone()
            print("data_nom_product ", data_nom_product, " type ", type(data_nom_product), " product ",
                  data_nom_product["Name_product"])

            session['data_product_delete'] = data_nom_product

            # Afficher la valeur sélectionnée dans le champ du formulaire "genre_delete_wtf.html"
            form_delete.Name_product_delete_wtf.data = data_nom_product["Name_product"]
            form_delete.Price_product_delete_wtf.data = data_nom_product["Price_product"]
            form_delete.Description_product_delete_wtf.data = data_nom_product["Description_product"]
            form_delete.ID_product_delete_wtf.data = data_nom_product["ID_product"]

            # Le bouton pour l'action "DELETE" dans le form. "product_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans Product_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans Product_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans Product_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans Product_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("Products/Product_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_films_associes=data_nom_product)

