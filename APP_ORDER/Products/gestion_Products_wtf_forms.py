"""
    Fichier : gestion_products_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""

from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import IntegerField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp



class FormWTFAjouterProducts(FlaskForm):
    """
        Dans le formulaire "products_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    Name_product_wtf = StringField("Product name")
    Price_product_wtf = StringField("Product price")
    Description_product_wtf = StringField("Product description ")

    submit = SubmitField("Enregistrer product")

class FormWTFUpdateProduct(FlaskForm):
    """
        Dans le formulaire "products_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    ID_product_wtf = StringField("ID_product")
    Name_product_wtf = StringField("Name_product")
    Price_product_wtf = StringField("Price_product")
    Description_product_wtf = StringField("Description_product")


    submit = SubmitField("Mise à jour product")

class FormWTFDeleteProduct(FlaskForm):
    """
        Dans le formulaire "product_delete_wtf.html"

        nom_product_delete_wtf : Champ qui reçoit la valeur du product, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "product".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_product".
    """
    id_product_delete_wtf = IntegerField("Effacer ce product")
    submit_btn_del = SubmitField("Effacer product")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
